Data Formats
===================


This document explains the default file formats used for different data like unaries, tracks, optical-flow and edge-maps. Please refer to the **IO** module in the source code for more detail. Also its should be straightforward to add support for a differently formatted file type by modifying the corresponding source code in **IO** module.

------------
Unaries
-------------
We expect the unaries to be dense per pixel probabilities. So for each pixel, we have a vector of probability for the pixel being assigned the corresponding label. If your classifier outputs scores, you need to apply [softmax function](https://en.wikipedia.org/wiki/Softmax_function) to convert them to probability scores between (0-1). In our code, we load a per image unary data as matrix of size: `number_of_labels x number_of_pixels`. So each column of the unary matrix is the probability vector for a particular pixel and they sum to one.

Bytes   | Contents
:------:|:------------------------------ 
 0-3    |  image **width** as an integer
 4-7    |  image **height** as an integer
 8-11   |  **number_of_labels** as an integer
12-end  |  **unary_data**  ( width* height* number_of_labels * 4 bytes total). Each unary probability vector of a pixel has number_of_labels float (4 bytes) values. Data is stored in row-major order. So we have the following order: unary[row0,col0],  unary[row0,col1], unary[row0,col2],...

For larger images, you may want to use some compressed unary format. In all those cases you have to modify `UnaryIO.h` and `UnaryIO.cpp` in the **IO** module to add a custom format.

-------------
Optical Flow
-------------

Currently, only [Middlebury Flow format ](http://vision.middlebury.edu/flow/code/flow-code/README.txt) support has been implemented.

-------------
Tracks
-------------

Tracks are represented via **Track2D** class. The default format supported by the [VideoParsing] library is the boost serialization of  `std::vector<Track2D>`.

The library also directly reads the text based output of the optical-flow based dense tracking [code](http://lmb.informatik.uni-freiburg.de/resources/binaries/eccv2010_trackingLinux64.zip) of [Sundaram et. al. ECCV 2010](http://dl.acm.org/citation.cfm?id=1886063.1886097). There is app (inportOFTracks.cpp) which converts from this text file format to a more compact and faster binary boost serialized format.

If you have your own fancy point tracker, you need to save them as one of the above formats.

-------------
Edge-Maps
-------------

Edge-maps are expected to be single-channel floating-point (32-bit) images, with values ranging from 0-1, with pixel value 1 indicating a edge pixel.

So, we can use image formats like [OpenEXR] which supports 32-bit images. We **recommend** [OpenEXR] for edge-maps. See the DemoSeq/Edges data for example. 

Other alternative is to use uncompressed binary file with following format:

Bytes   | Contents
:------:|:------------------------------ 
 0-3    |  image **width** as an integer
 4-7    |  image **height** as an integer
 8-11   |  **4** as an integer
12-end  |  **edge_probability**  ( width* height* 4 bytes total). Each pixel probability pixel has 4 bytes (32 bit float). Data is stored in row-major order. So we have the following order: prob[row0,col0], prob[row0,col1], prob[row0,col2],...


  [OpenEXR]: https://en.wikipedia.org/wiki/OpenEXR
  [VideoParsing]: https://bitbucket.org/infinitei/videoparsing